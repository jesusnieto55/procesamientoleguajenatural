import nltk
import re
from PackageDownload import try_download_package


try_download_package('tokenizers/punkt', 'punkt')
try_download_package('taggers/averaged_perceptron_tagger', 'averaged_perceptron_tagger')


def tokenize(text):
    return nltk.word_tokenize(text)


def clean(text):
    """
    Removes punctuation symbols and converts to lowercase
    """
    pattern = r'[,":%\.\-\(\)\?\!\;\@\_\$\#\^\&\*\[\]\{\}\>\<\{\}]'
    cleaned = re.sub(pattern, '', text)
    cleaned = cleaned.lower()
    return cleaned


def grammatical_filter(tokens):
    # Full list of grammatical tags: https://www.guru99.com/pos-tagging-chunking-nltk.html
    # View list of grammatical tags: nltk.help.upenn_tagset()
    allowed_tags = ['CD', 'FW', 'JJ', 'JJR', 'JJS', 'LS', 'NN', 'NNS', 'NNP', 'NNPS', 'RB', 'RBR', 'RBS', 'VB', 'VBG', 'VBD', 'VBN', 'VBP', 'VBZ', 'WDT', 'WP', 'WRB']
    tokens_tagged = nltk.pos_tag(tokens)
    tokens_filtered = [t[0] for t in tokens_tagged if t[1] in allowed_tags]
    return tokens_filtered


def preprocess(texts):
    tokenized_texts = []
    for text in texts:
        cleaned_text = clean(text)
        tokens = tokenize(cleaned_text)
        tokens = grammatical_filter(tokens)
        tokenized_texts.append(tokens)
    return tokenized_texts
