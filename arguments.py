import argparse
from enum import Enum

from Models.OntologyBasedModel import OntologySimilarity, Corpus, OntologyBasedModel
from Models.TextSimilarityModel import AguirreTextSimilarityModel, MihalceaTextSimilarityModel
from Models.DistributionalBasedModel import Distance, DistributionalBasedModel


class TextSimilarityMethod(Enum):
    AGUIRRE = 'Aguirre'
    MIHALCEA = 'Mihalcea'


class WordSimilarityMethod(Enum):
    ONTOLOGY = 'ontology'
    DISTRIBUTIONAL = 'distributional'


class DataSet(Enum):
    TEST = 'test'
    TRAIN = 'train'


class DataSetSubType(Enum):
    MSRPAR = 'MSRpar'
    HEADLINES = 'headlines'
    DEFTNEWS = 'deft-news'
    MSRVID = 'MSRvid'
    IMAGES = 'images'
    TRACK = 'track5.en-en'
    DEFTFORUM = 'deft-forum'
    ANSWERS = 'answers-forums'
    ANSWER_ANSWER = 'answer-answer'


class Arguments:
    def __init__(self):
        self.textSimilarityModel = None
        self.wordSimilarityModel = None
        self.dataset = None
        self.dataset_subtype = None
        self.verbose = None
        self.threshold = None

        self.__initialize_parser__()

    def __initialize_parser__(self):
        self.parser = argparse.ArgumentParser()

        self.parser.add_argument('-m', '--method',
                                 type=str,
                                 help='Method to be used to calculate text similarity',
                                 default=TextSimilarityMethod.AGUIRRE.value,
                                 choices=[x.value for x in list(TextSimilarityMethod)])
        self.parser.add_argument('-wsm', '--wordSimilarityMethod',
                                 type=str,
                                 help='Method to be used for word similarity. It can be ontology based or distributional based',
                                 default=WordSimilarityMethod.ONTOLOGY.value,
                                 choices=[x.value for x in list(WordSimilarityMethod)])
        self.parser.add_argument('-s', '--similarity',
                                 type=str,
                                 help='Word similarity function to be used when selecting ontology based similarity',
                                 default=OntologySimilarity.SHORTEST_PATH.value,
                                 choices=[x.value for x in list(OntologySimilarity)])
        self.parser.add_argument('-t', '--threshold',
                                 type=float,
                                 help='Threshold to be used when aligning words, this is only applicable to Aguirre method.',
                                 default=1)
        self.parser.add_argument('-d', '--distance',
                                 type=str,
                                 help='Distance metric to be used when selecting distributional based similarity',
                                 default=Distance.COSINE.value,
                                 choices=[x.value for x in list(Distance)])
        self.parser.add_argument('-c', '--corpus',
                                 type=str,
                                 help='Corpus to be used in word similarity function. This is only applicable when using an ontology based method',
                                 default=Corpus.WORDNET.value,
                                 choices=[x.value for x in list(Corpus)])
        self.parser.add_argument('-ds', '--dataset',
                                 type=str,
                                 help='Dataset to be used',
                                 default=DataSet.TRAIN.value,
                                 choices=[x.value for x in list(DataSet)])
        self.parser.add_argument('-dst', '--dataset-subtype',
                                 type=str,
                                 help='Dataset sub-type to be used',
                                 choices=[x.value for x in list(DataSetSubType)])
        self.parser.add_argument('-v', '--verbose', action='store_true')

    def parse(self):
        raw_arguments = self.parser.parse_args()

        self.__print_arguments__(raw_arguments)

        text_method = self.__parse_enum__(TextSimilarityMethod, raw_arguments.method)
        word_method = self.__parse_enum__(WordSimilarityMethod, raw_arguments.wordSimilarityMethod)
        similarity = self.__parse_enum__(OntologySimilarity, raw_arguments.similarity)
        distance = self.__parse_enum__(Distance, raw_arguments.distance)
        corpus = self.__parse_enum__(Corpus, raw_arguments.corpus)

        self.dataset = self.__parse_enum__(DataSet, raw_arguments.dataset)
        self.dataset_subtype = self.__parse_enum__(DataSetSubType, raw_arguments.dataset_subtype)
        self.verbose = raw_arguments.verbose
        self.threshold = raw_arguments.threshold

        if text_method == TextSimilarityMethod.AGUIRRE:
            self.textSimilarityModel = AguirreTextSimilarityModel()
        elif text_method == TextSimilarityMethod.MIHALCEA:
            self.textSimilarityModel = MihalceaTextSimilarityModel()

        if word_method == WordSimilarityMethod.ONTOLOGY:
            self.wordSimilarityModel = OntologyBasedModel(metric=similarity, corpus=corpus)
        elif word_method == WordSimilarityMethod.DISTRIBUTIONAL:
            self.wordSimilarityModel = DistributionalBasedModel(metric=distance)

    @staticmethod
    def __parse_enum__(enum: Enum, value: str):
        if value is None:
            return None

        for item in (enum):
            if item.value.endswith(value):
                return item
        return None

    @staticmethod
    def __print_arguments__(arguments):
        args_to_print = ', '.join(f'{k}={v}' for k, v in vars(arguments).items())
        print(f'Executing with the following arguments: {args_to_print}')
