import nltk


def try_download_package(find_package, download_package):
    try:
        nltk.data.find(find_package)
    except LookupError:
        nltk.download(download_package)
