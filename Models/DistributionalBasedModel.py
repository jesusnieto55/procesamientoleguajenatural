from gensim import models
import gensim.downloader
from enum import Enum
import numpy as np

from Models.OntologyBasedModel import WordSimilarityModel


class Distance(Enum):
    EXACT_WORD = 'exact_word'
    COSINE = 'cosine'
    EUCLIDEAN_DISTANCE = 'euclidean'
    SCALAR_PRODUCT = 'scalar'


class DistributionalBasedModel(WordSimilarityModel):
    def __init__(self, metric: Distance = Distance.COSINE):
        self.metric = metric
        self.model = None

    def words_similarity(self, tokenized_text1=[], tokenized_text2=[]):
        similarities = []
        for (i, word1) in enumerate(tokenized_text1):
            similarity = 0
            if i < len(tokenized_text2):
                word2 = tokenized_text2[i]
                if word1 is not None and word2 is not None:
                    similarity = self.word_similarity(word1, word2)
            similarities.append(similarity)
        return similarities

    def word_similarity(self, word1, word2):
        if self.metric == Distance.EXACT_WORD:
            return self.__exact_word_similarity__(word1, word2)

        if self.model is None:
            print('Loading pre-trained Word2Vec model containing 3 million words. If this is the first execution, '
                  'it will download the file, which might take some time...')
            self.model = gensim.downloader.load('word2vec-google-news-300')

        vector1 = self.__get_vector_from_word__(word1)
        vector2 = self.__get_vector_from_word__(word2)
        if vector1 is None or vector2 is None:
            return self.__exact_word_similarity__(word1, word2)

        if self.metric == Distance.COSINE:
            return self.__cosine_distance_similarity__(vector1, vector2)
        elif self.metric == Distance.EUCLIDEAN_DISTANCE:
            return self.__euclidean_distance_similarity__(vector1, vector2)
        elif self.metric == Distance.SCALAR_PRODUCT:
            return self.__scalar_product_similarity__(vector1, vector2)

    def __get_vector_from_word__(self, word):
        return self.model[word] if self.model.has_index_for(word) else None

    @staticmethod
    def __exact_word_similarity__(word1, word2):
        return 1 if word1 == word2 else 0

    def __cosine_distance_similarity__(self, vector1: [], vector2: []):
        cosine_similarities = self.model.cosine_similarities(vector1, [vector2])
        similarity = cosine_similarities[0] if len(cosine_similarities) > 0 else 0
        return similarity

    @staticmethod
    def __euclidean_distance_similarity__(vector1: [], vector2: []):
        euclidean_distance = np.linalg.norm(vector1 - vector2)
        return 1/euclidean_distance if euclidean_distance != 0 else 1

    @staticmethod
    def __scalar_product_similarity__(vector1, vector2):
        return np.dot(vector1, vector2)
