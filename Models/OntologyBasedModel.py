from nltk.corpus import wordnet, genesis, gutenberg, webtext, reuters, wordnet_ic
from enum import Enum
from PackageDownload import try_download_package

try_download_package('corpora/wordnet', 'wordnet')
try_download_package('corpora/wordnet_ic', 'wordnet_ic')
try_download_package('corpora/genesis', 'genesis')
try_download_package('corpora/gutenberg', 'gutenberg')
try_download_package('corpora/webtext', 'webtext')
try_download_package('corpora/brown', 'brown')
try_download_package('corpora/reuters', 'reuters')


class OntologySimilarity(Enum):
    SHORTEST_PATH = 'shortest_path'
    LEACOCK_CHODOROW = 'leacock_chodrow'
    WU_PALMER = 'wu_palmer'
    RESNIK = 'resnik'
    JIAN_CONRATH = 'jian_conrath'
    LIN = 'lin'


class Corpus(Enum):
    WORDNET = 'wordnet'
    GENESIS = 'genesis'
    GUTENBERG = 'gutenberg'
    WEBTEXT = 'webtext'
    BROWN = 'brown'
    REUTERS = 'reuters'


class WordSimilarityModel:
    def word_similarity(self, word1, word2):
        pass


class OntologyBasedModel(WordSimilarityModel):
    def __init__(self, metric: OntologySimilarity = OntologySimilarity.SHORTEST_PATH, corpus: Corpus = Corpus.WORDNET):
        """
        Not all combinations of metric and corpora are allowed. Only RESNIK, JIAN_CONRATH and LIN allow a different
        corpus than WORDNET.
        :param metric:
        :param corpus:
        """
        self.metric = metric
        self.corpora = corpus
        self.__initialize_ic__()

    def __initialize_ic__(self):
        if self.corpora == Corpus.WORDNET:
            self.ic = wordnet.ic(wordnet)
        elif self.corpora == Corpus.GENESIS:
            self.ic = wordnet.ic(genesis, False, 0.0)
        elif self.corpora == Corpus.GUTENBERG:
            self.ic = wordnet.ic(gutenberg, False, 0.0)
        elif self.corpora == Corpus.WEBTEXT:
            self.ic = wordnet.ic(webtext, False, 0.0)
        elif self.corpora == Corpus.BROWN:
            self.ic = wordnet_ic.ic('ic-brown.dat')
        elif self.corpora == Corpus.REUTERS:
            self.ic = wordnet.ic(reuters, False, 0.0)

    def words_similarity(self, tokenized_text1=[], tokenized_text2=[]):
        similarities = []
        for (i, word) in enumerate(tokenized_text1):
            similarity = 0
            if i < len(tokenized_text2):
                similarity = self.word_similarity(word, tokenized_text2[i])
            similarities.append(similarity)
        return similarities

    def word_similarity(self, word1, word2):
        corpus_word1 = self.__get_corpus_word__(word1)
        corpus_word2 = self.__get_corpus_word__(word2)
        if corpus_word1 is None or corpus_word2 is None:
            return self.__exact_word_similarity__(word1, word2)

        similarity = self.__get_similarity__(corpus_word1, corpus_word2)
        return similarity if similarity is not None else 0

    @staticmethod
    def __get_corpus_word__(word):
        synset = wordnet.synsets(word)
        return synset[0] if len(synset) > 0 else None

    def __get_similarity__(self, corpus_word1, corpus_word2):
        if self.metric == OntologySimilarity.SHORTEST_PATH:
            return corpus_word1.path_similarity(corpus_word2)

        elif self.metric == OntologySimilarity.LEACOCK_CHODOROW:
            if not self.__same_part_of_speech__(corpus_word1, corpus_word2):
                return 0
            return corpus_word1.lch_similarity(corpus_word2)

        elif self.metric == OntologySimilarity.WU_PALMER:
            return corpus_word1.wup_similarity(corpus_word2)

        elif self.metric == OntologySimilarity.RESNIK:
            return self.__resnik_similarity__(corpus_word1, corpus_word2)

        elif self.metric == OntologySimilarity.JIAN_CONRATH:
            return self.__jian_conrath_similarity__(corpus_word1, corpus_word2)

        elif self.metric == OntologySimilarity.LIN:
            return self.__lin_similarity__(corpus_word1, corpus_word2)

        else:
            return None

    def __resnik_similarity__(self, corpus_word1, corpus_word2):
        if not self.__same_part_of_speech__(corpus_word1, corpus_word2):
            return 0
        if corpus_word1.pos() not in self.ic or corpus_word2.pos() not in self.ic:
            return 0
        return corpus_word1.res_similarity(corpus_word2, ic=self.ic)

    def __jian_conrath_similarity__(self, corpus_word1, corpus_word2):
        if not self.__same_part_of_speech__(corpus_word1, corpus_word2):
            return 0
        if corpus_word1.pos() not in self.ic or corpus_word2.pos() not in self.ic:
            return 0
        return corpus_word1.jcn_similarity(corpus_word2, ic=self.ic)

    def __lin_similarity__(self, corpus_word1, corpus_word2):
        if not self.__same_part_of_speech__(corpus_word1, corpus_word2):
            return 0
        if corpus_word1.pos() not in self.ic or corpus_word2.pos() not in self.ic:
            return 0
        try:
            return corpus_word1.lin_similarity(corpus_word2, ic=self.ic)
        except ZeroDivisionError:
            return 0

    @staticmethod
    def __same_part_of_speech__(corpus_word1, corpus_word2):
        return corpus_word1.pos() == corpus_word2.pos()

    @staticmethod
    def __exact_word_similarity__(word1, word2):
        return 1 if word1 == word2 else 0
