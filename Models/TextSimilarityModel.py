from nltk.corpus import brown
import math
import nltk
from typing import List, Callable
from PackageDownload import try_download_package

try_download_package('corpora/brown', 'brown')


class TextSimilarityModel:
    def text_similarity(self, tokenized_text1: List[str], tokenized_text2: List[str],
                        word_similarity: Callable[[str, str], float], threshold: float):
        pass


class MihalceaTextSimilarityModel(TextSimilarityModel):
    def __init__(self):
        self.corpus_size = None
        self.frequency_distribution = None

    def idf(self, term: str) -> float:
        """
        Calculates the Inverse Document Frequency of a term in a specific corpus (Brown).
        https://en.wikipedia.org/wiki/Tf%E2%80%93idf
        :param term:
        :return:
        """
        if self.corpus_size is None:
            self.corpus_size = len(brown.words())
            self.frequency_distribution = nltk.FreqDist(brown.words())

        word_frequency = self.frequency_distribution[term]
        return math.log(self.corpus_size / (1 + word_frequency))

    def text_similarity(self, tokenized_text1: List[str], tokenized_text2: List[str],
                        word_similarity: Callable[[str, str], float], threshold: float):
        """
        This applies the formula (1) defined by Mihalcea et al. 2006 for text semantic similarity in
        https://www.aaai.org/Papers/AAAI/2006/AAAI06-123.pdf
        :param tokenized_text1: first tokenized text to be compared
        :param tokenized_text2: second tokenized text to be compared
        :param word_similarity: function to calculate the similarity between two words
        :return: a float indicating the semantic similarity between the two texts
        """
        term1 = sum(
            [self.idf(w1) * word_similarity(w1, max(tokenized_text2, key=lambda w2: word_similarity(w1, w2))) for w1 in
             tokenized_text1])
        term2 = sum(
            [self.idf(w1) * word_similarity(w1, max(tokenized_text1, key=lambda w2: word_similarity(w1, w2))) for w1 in
             tokenized_text2])

        text1_idf_sum = sum([self.idf(w) for w in tokenized_text1])
        text2_idf_sum = sum([self.idf(w) for w in tokenized_text2])

        return 0.5 * ((term1 / text1_idf_sum) + (term2 / text2_idf_sum))


class AguirreTextSimilarityModel(TextSimilarityModel):
    def text_similarity(self, tokenized_text1: List[str], tokenized_text2: List[str],
                        word_similarity: Callable[[str, str], float] = None, threshold: float = 1):
        """
        This applies the formula (2.6) defined by Aitor Gonzalez-Aguirre in his PhD Thesis for text semantic similarity
        in https://addi.ehu.es/bitstream/handle/10810/23867/TESIS_GONZALEZ_AGIRRE_AITOR.pdf?sequence=1
        :param tokenized_text1: first tokenized text to be compared
        :param tokenized_text2: second tokenized text to be compared
        :param word_similarity: function to calculate the similarity between two words. If not specified, it uses
               plain word comparison.
        :param threshold: threshold to accept word overlap between two words. Default is 1.
        :return: a float indicating the semantic similarity between the two texts
        :param tokenized_text1:
        :param tokenized_text2:
        :param word_similarity:
        :return: a float indicating the semantic similarity between the two texts
        """
        aligned = self.__get_aligned__(tokenized_text1, tokenized_text2, word_similarity, threshold)

        similarity = (2 * len(aligned)) / (len(tokenized_text1) + len(tokenized_text2))

        return similarity

    @staticmethod
    def __get_aligned__(tokenized_text1: List[str], tokenized_text2: List[str],
                        word_similarity: Callable[[str, str], float] = None, threshold: float = 1):
        aligned = []
        for (i, word1) in enumerate(tokenized_text1):
            for (j, word2) in enumerate(tokenized_text2):
                similarity = word_similarity(word1, word2)
                if similarity >= threshold:
                    aligned.append((word1, word2))
        return aligned
