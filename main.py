import DataReader
import DataPreprocessor
from arguments import Arguments
from metrics import pearson_correlation, normalize

# Read console arguments
args = Arguments()
args.parse()

# Read and preprocess data
texts1, text2, true_similarities = DataReader.get_data(args.dataset, args.dataset_subtype)
tokenized_texts1 = DataPreprocessor.preprocess(texts1)
tokenized_texts2 = DataPreprocessor.preprocess(text2)

# Execute for all lines
similarities_predicted = []
for (i, text) in enumerate(tokenized_texts1):
    text1 = text
    text2 = tokenized_texts2[i]
    true_similarity = true_similarities[i]
    similarity = args.textSimilarityModel.text_similarity(text1, text2, args.wordSimilarityModel.word_similarity, args.threshold)
    if args.verbose:
        print(f'line {i} - Real Sim:{true_similarity}, Estim. Sim:({similarity}): {text1} -- {text2}')

    similarities_predicted.append(similarity)

# Show result
normalized_predictions = normalize(similarities_predicted)
pearson = pearson_correlation(true_similarities, normalized_predictions)

print('----------------------- Result -------------------------')
print(f'Pearson Correlation: {pearson}')

