import csv

from arguments import DataSetSubType, DataSet


def __read_data__(file_name: str, subtype: DataSetSubType = None):
    texts1 = []
    texts2 = []
    similarities = []
    with open(file_name, encoding="utf8") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter='\t', quotechar=None)
        for row in csv_reader:
            if subtype is None or subtype.value == row[1]:
                texts1.append(row[5])
                texts2.append(row[6])
                similarities.append(parse_similarity(row[4]))
    return texts1, texts2, similarities


def parse_similarity(similarity: str) -> float:
    float_similarity = float(similarity)
    return float_similarity / 5


def get_data(dataset: DataSet, subtype: DataSetSubType):
    if dataset == DataSet.TRAIN:
        return get_train_data(subtype)
    elif dataset == DataSet.TEST:
        return get_test_data(subtype)


def get_train_data(subtype: DataSetSubType = None):
    return __read_data__('data/sts-train.csv', subtype)


def get_test_data(subtype: DataSetSubType = None):
    return __read_data__('data/sts-test.csv', subtype)


def get_dev_data(subtype: DataSetSubType = None):
    return __read_data__('data/sts-dev.csv', subtype)
