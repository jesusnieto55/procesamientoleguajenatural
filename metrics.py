from scipy.stats import pearsonr
from sklearn import preprocessing


def pearson_correlation(data1: [], data2: []):
    corr, _ = pearsonr(data1, data2)
    return corr


def normalize(data: []):
    return preprocessing.normalize([data], norm='max')[0]
